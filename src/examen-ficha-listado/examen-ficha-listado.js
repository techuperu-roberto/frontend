import {LitElement, html} from 'lit-element';

class ExamenFichaListado extends LitElement {

    static get properties(){
        return {
            questionsID: {type: Number},
            detail: {type: String},
            answerOptions: {type: Array}
        };
    }

    constructor(){
        super();
    }

    render() {
        return html `
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
 
            <div class="card" id="cardPregunta">
                <div class="card-body" id="cardCuerpo">
                    <h4 class="card-title">${this.questionsID}</h4>
                    <p class="card-text">${this.detail}</p>

                    <div class="form-check">
                        <label class="form-check-label" for="radio1">
                            <input type="radio" class="form-check-input" id="radio1" name="optradio" value="option1">${this.answerOptions[0].Opcion}
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label" for="radio2">
                            <input type="radio" class="form-check-input" id="radio2" name="optradio" value="option2">${this.answerOptions[1].Opcion}
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label" for="radio3">
                            <input type="radio" class="form-check-input" id="radio3" name="optradio" value="option3">${this.answerOptions[2].Opcion}
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label" for="radio4">
                            <input type="radio" class="form-check-input" id="radio4" name="optradio" value="option4">${this.answerOptions[3].Opcion}
                        </label>
                    </div>
                    <div class="form-check">
                        <label class="form-check-label" for="radio5">
                            <input type="radio" class="form-check-input" id="radio5" name="optradio" value="option5">${this.answerOptions[4].Opcion}
                        </label>
                    </div>
                </div>
            </div> 
        `;
    }


}

customElements.define('examen-ficha-listado', ExamenFichaListado); 