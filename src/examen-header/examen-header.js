import {LitElement, html} from 'lit-element';

class ExamenHeader extends LitElement {

    static get properties(){
        return {

        };
    }

    constructor(){
        super();
    }

    render() {
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <div class="jumbotron jumbotron-fluid">
                <div class="container">
                    <h1 class="display-3">Exámenes BBVA</h1>
                    <p class="lead">Sitio web para rendir exámenes</p>
                    <hr class="my-2">
                    <p>TechU 2020</p>
                </div>
            </div>
        `;
    }
}

customElements.define('examen-header', ExamenHeader); 

