import {LitElement, html} from 'lit-element';

class ExamenSidebar extends LitElement {

    static get properties(){
        return {
        };
    }

    constructor(){
        super();
    }

    render() {
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <aside>
                <section>
                    <div class="mt-5">
                        <button @click="${this.newExamen}" class="w-100 btn bg-primary text-white"><strong>Rendir examen</strong></button>
                    </div>
                </section>
            </aside>
        `;
    }

    newExamen(e){
        console.log("newExamen en examen-sidebar");
        console.log("Se va a crear un nuevo registro de Examen");
        this.dispatchEvent(new CustomEvent("new-Examen", {}));
    
    }
}

customElements.define('examen-sidebar', ExamenSidebar); 