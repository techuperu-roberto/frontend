import {LitElement, html} from 'lit-element';
import '../examen-header/examen-header'
import '../examen-main/examen-main'
import '../examen-footer/examen-footer'
import '../examen-sidebar/examen-sidebar'
import '../examen-stats/examen-stats'

class ExamenApp extends LitElement {

    static get properties(){
        return {
            people: {type: Array}
        };
    }

    constructor(){
        super();
    }

    render() {
        return html `

            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <examen-header></examen-header>
            <div class="row">
                <examen-sidebar id="peopleSidebar" @new-Examen="${this.newExamen}" class="col-2"></examen-sidebar>
                <examen-main @update-people="${this.updatePeople}" class="col-10"></examen-main>
            </div>
            <examen-footer></examen-footer>
            <examen-stats @updated-people-stats="${this.peopleStatsUpdated}"></examen-stats>
        `;
    }

    newExamen(e){
        console.log("newExamen en examen-app"); 
        this.shadowRoot.querySelector("examen-main").showExamenForm = true;

        //TODO llamar al servicio de consulta de preguntas
        this.consultarServicioPreguntas();
    }

    consultarServicioPreguntas(){

        console.log("consultarServicioPreguntas");

        let xhr = new XMLHttpRequest();
        let preguntas = [];
        xhr.onload = () => {
            if(xhr.status === 200){
                console.log("Petición completada correctamente");               
                preguntas = JSON.parse(xhr.responseText);
                this.shadowRoot.querySelector("examen-main").people = preguntas;
            }
        }

        xhr.open("GET", "https://techuprueba.herokuapp.com/questions/v1/random/5/");

        xhr.send();

    }


    peopleStatsUpdated(e){
        console.log("peopleStatsUpdated en examen-app", e.detail);

        this.shadowRoot.querySelector("examen-sidebar").peopleStats = e.detail.peopleStats;
    }

    updatePeople(e){
        console.log("updatePeople en examen-app", e.detail);
        this.people = e.detail.people;
    }

    updated(changedProperties){
        console.log("updated en examen-app", changedProperties);

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en examen-app");
            this.shadowRoot.querySelector("examen-stats").people = this.people;
        }
    }
}

customElements.define('examen-app', ExamenApp); 