import {LitElement, html} from 'lit-element';

class ExamenStats extends LitElement {

    static get properties(){
        return {
            people: {type: Array}
        };
    }

    constructor(){
        super();
        this.people = [];
    }

    updated(changedProperties){
        console.log("updated en examen-stats");
        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en examen-stats", this.people);
            let peopleStats = this.gatherPeopleArrayInfo(this.people);

            this.dispatchEvent(new CustomEvent("updated-people-stats",{
                detail: {
                    peopleStats
                }
            }));
        }
    }

    gatherPeopleArrayInfo(people){
        let peopleStats ={};
        peopleStats.numberOfPeople = people.length;
        return peopleStats;
    }

}

customElements.define('examen-stats', ExamenStats); 