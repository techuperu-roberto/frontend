import {LitElement, html, css} from 'lit-element';
import '../examen-ficha-listado/examen-ficha-listado';
import '../examen-bienvenida/examen-bienvenida';
import '../examen-resultado/examen-resultado';

class ExamenMain extends LitElement {

    static get properties(){
        return {
            people: {type: Array},
            showExamenForm: {type: Boolean},
            resultados: {type: Array}
        };
    }

    constructor(){
        super();

        this.showExamenForm = false; 
        this.resultados = [];
        this.people = [];

     }

    render() {
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <main>
                <div class="container" id="peopleList">
                    <div class="display-4 text-center">Examen generado</div>
                    <br/>
                        ${this.people.map(
                            Examen => html`<examen-ficha-listado id="${Examen.questionsID}" questionsID="${Examen.questionsID}" detail="${Examen.detail}" .answerOptions="${Examen.answerOptions}" @delete-Examen="${this.deleteExamen}" @info-Examen="${this.infoExamen}"></examen-ficha-listado>` 
                        )}
                    <br/>
                    <button @click="${this.cancelarExamen}" class="btn btn-secondary col-5"><strong>Cancelar</strong></button>
                    <button @click="${this.enviarExamen}" class="btn btn-primary col-5 offset-1"><strong>Enviar examen</strong></button>
                    <br/>
                    <br/>
                </div>

                <div class="container">
                    <examen-bienvenida id="examenBienvenida"></examen-bienvenida>
                </div>
                <div class="container">
                    <examen-resultado id="examenResultado" .resultados="${this.resultados}"></examen-resultado>
                </div>
                        
            </main>


        `;
    }

    cancelarExamen(){
        console.log("cancelarExamen en examen-main");
        this.showExamenForm = false;
    }

    enviarExamen(){
        console.log("enviarExamen en examen-main");
        var respuestas = [];
        for (let index = 0; index < this.people.length; index++) {
            console.log("recorre preguntas marcadas:");
            if(this.shadowRoot.getElementById(this.people[index].questionsID).shadowRoot.getElementById("radio1").checked){
                respuestas.push({
                    questionsID: this.people[index].questionsID,
                    answerID: 1
                });
            }else if(this.shadowRoot.getElementById(this.people[index].questionsID).shadowRoot.getElementById("radio2").checked){
                respuestas.push({
                    questionsID: this.people[index].questionsID,
                    answerID: 2
                });
            }else if(this.shadowRoot.getElementById(this.people[index].questionsID).shadowRoot.getElementById("radio3").checked){
                respuestas.push({
                    questionsID: this.people[index].questionsID,
                    answerID: 3
                });
            }else if(this.shadowRoot.getElementById(this.people[index].questionsID).shadowRoot.getElementById("radio4").checked){
                respuestas.push({
                    questionsID: this.people[index].questionsID,
                    answerID: 4
                });
            }else if(this.shadowRoot.getElementById(this.people[index].questionsID).shadowRoot.getElementById("radio5").checked){
                respuestas.push({
                    questionsID: this.people[index].questionsID,
                    answerID: 5
                });
            } else {
                respuestas.push({
                    questionsID: this.people[index].questionsID,
                    answerID: 0
                });

            }           
        }
        console.log("Respuestas:",respuestas);

        this.consultarServicioRespuestas(respuestas);
        
        this.showExamenResultado();
    }

    consultarServicioRespuestas(r){

        //TODO llamar a servicio POST

        console.log("consultarServicioRespuestas", r);

        let xhr = new XMLHttpRequest();

        var data = JSON.stringify({
            "email":"mmoquillaza@gmail.com",
            "answers": r
        });

        xhr.onload = () => {
            if(xhr.status === 200){
                console.log("Petición completada correctamente");               
                this.resultados = JSON.parse(xhr.responseText);
                console.log(this.resultados);
            }
        }

        xhr.open("POST", "https://techuprueba.herokuapp.com/answers");
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(data);
    
    }

    updated(changedProperties){
        console.log("updated");
        if(changedProperties.has("showExamenForm")){
            console.log("Ha cambiado el valor de showExamenForm en examen-main");
            if(this.showExamenForm === true){
                this.showExamenFormData();
            } else {
                this.showExamenList();
            }
        }

        if (changedProperties.has("people")) {
            console.log("Ha cambiado el valor de la propiedad people en examen-main", this.people);
            this.dispatchEvent(new CustomEvent("update-people",{
                detail:{
                    people: this.people
                }
            }));
        }
    }

    showExamenList(){
        console.log("showExamenList");
        console.log("Mostrando listado de Exámenes");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById("examenBienvenida").classList.remove("d-none");
        this.shadowRoot.getElementById("examenResultado").classList.add("d-none");
    }

    showExamenFormData(){
        console.log("showExamenFormData");
        console.log("Mostrando formulario de Examen");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("examenBienvenida").classList.add("d-none");
        this.shadowRoot.getElementById("examenResultado").classList.add("d-none");

    }
    showExamenResultado(){
        console.log("showExamenResultado");
        console.log("Mostrando resultado de Examen");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
        this.shadowRoot.getElementById("examenBienvenida").classList.add("d-none");
        this.shadowRoot.getElementById("examenResultado").classList.remove("d-none");

    }
}

customElements.define('examen-main', ExamenMain); 