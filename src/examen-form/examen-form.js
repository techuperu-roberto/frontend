import {LitElement, html} from 'lit-element';

class ExamenForm extends LitElement {

    static get properties(){
        return {
            Examen: {type: Object},
            editingExamen: {type: Boolean}
        };
    }

    constructor(){
        super();
        this.resetFormData();
    }

    render() {
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre completo</label>
                        <input @input="${this.updateName}" type="text" id="ExamenFormName" class="form-control" placeholder="Ingrese su nombre completo" .value="${this.Examen.name}" ?disabled="${this.editingExamen}" />
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea @input="${this.updateProfile}" class="form-control" placeholder="Ingrese brevemente su perfil" rows="5" .value="${this.Examen.profile}"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input @input="${this.updateYearsInCompany}" type="text" class="form-control" placeholder="Ingrese su antigüedad" .value="${this.Examen.yearsInCompany}" />
                    </div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storeExamen}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }

    updateName(e){
        console.log("updateName");
        console.log("Actualizando la propiedad Name con el valor " + e.target.value);
        this.Examen.name = e.target.value;

    }
    updateProfile(e){
        console.log("updateProfile");
        console.log("Actualizando la propiedad Profile con el valor " + e.target.value);
        this.Examen.profile = e.target.value;

    }
    updateYearsInCompany(e){
        console.log("updateYearsInCompany");
        console.log("Actualizando la propiedad YearsInCompany con el valor " + e.target.value);
        this.Examen.yearsInCompany = e.target.value;

    }

    goBack(e){
        console.log("goBack en examen-form");
        e.preventDefault();
        this.dispatchEvent(new CustomEvent("examen-form-close", {}));
        this.resetFormData();
    }

    storeExamen(e){
        console.log("storeExamen en examen-form");
        e.preventDefault();

        this.Examen.photo = {
            "src": "./img/Examena.jpg",
            "alt": "Examena"
        }
        console.log(this.Examen);

        this.dispatchEvent(new CustomEvent("examen-form-store", {detail: 
            {Examen: {
                name: this.Examen.name,
                profile: this.Examen.profile,
                yearsInCompany: this.Examen.yearsInCompany,
                photo: this.Examen.photo
                }, editingExamen: this.editingExamen

            }
        }));
        this.resetFormData();
    }

    resetFormData(){
        console.log("resetFormData en examen-form");
        this.Examen = {};
        this.Examen.name = "";
        this.Examen.profile = "";
        this.Examen.yearsInCompany="";
        this.editingExamen = false;
    }
}

customElements.define('examen-form', ExamenForm); 