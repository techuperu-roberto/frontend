import {LitElement, html} from 'lit-element';

class ExamenFooter extends LitElement {

    static get properties(){
        return {

        };
    }

    constructor(){
        super();
    }

    render() {
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <h5 class="font-weight-bolder bg-secondary text-white">@ExamenApp 2020</h5>
        `;
    }
}

customElements.define('examen-footer', ExamenFooter); 