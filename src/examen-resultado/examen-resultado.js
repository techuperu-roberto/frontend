import {LitElement, html} from 'lit-element';

class ExamenResultado extends LitElement {

    static get properties(){
        return {
            resultados: {type:Object}
        };
    }

    constructor(){
        super();
        this.resultados = {
            "examenID" : 1234,
            "puntaje" : 10,
            "dictamen" : "APROBADO",
            "rightQuestions": 3,
            "wrongQuestions": 2
        };
    }

    render() {
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">
            <div class="container p-3 my-3 border bg-info text-white">
                <h1>Resultados del examen</h1>
                <br/>
                <h3>Dictamen: <span class="badge badge-primary">${this.resultados.dictamen}</span>.</h3>                
                <div>Examen ID: <span class="badge badge-primary">${this.resultados.examenID}</span>.</div>                
                <div>Puntaje final: <span class="badge badge-primary">${this.resultados.puntaje}</span>.</div>                
                <div>Total preguntas correctas: <span class="badge badge-success">${this.resultados.rightQuestions}</span>.</div>                
                <div>Total preguntas incorrectas: <span class="badge badge-danger">${this.resultados.wrongQuestions}</span>.</div>                
            </div>
            <br/>
            <br/>
            <br/>
            <br/>
            <br/>

        `;
    }

}

customElements.define('examen-resultado', ExamenResultado); 