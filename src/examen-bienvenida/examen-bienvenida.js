import {LitElement, html} from 'lit-element';

class ExamenBienvenida extends LitElement {

    static get properties(){
        return {

        };
    }

    constructor(){
        super();
    }

    render() {
        return html `
            <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/css/bootstrap.min.css" integrity="sha384-TX8t27EcRE3e/ihU7zmQxVncDAy5uIKz4rEkgIXeMed4M0jlfIDPvg6uqKI2xXr2" crossorigin="anonymous">

            <div class="container p-3 my-3 border">
                <h1>¡Bienvenido!</h1>
                <p>En este site puede rendir los exámenes que requiera.</p>
                <p>Seleccione el botón "Rendir" para que se pueda generar un examen para usted.</p>
            </div>
        `;
    }
}

customElements.define('examen-bienvenida', ExamenBienvenida); 